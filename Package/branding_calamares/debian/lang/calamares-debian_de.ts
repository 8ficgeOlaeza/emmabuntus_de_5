<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>show</name>
    <message>
        <location filename="../show.qml" line="45"/>
        <source>Welcome to Debian 12 GNU/Linux - Emmabuntus DE 5.&lt;br/&gt;The rest of the installation is automated and should complete in a few minutes.</source>
        <translation>Willkommen in Debian 12 GNU/Linux - Emmabuntüs DE 5.&lt;br/&gt;Der Rest der Installation ist automatisiert und sollte in ein paar Minuten fertiggestellt werden.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="67"/>
        <source>More customization available through the post-installation process.</source>
        <translation>Mehr Personalisierung dank der Nachinstallation.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="88"/>
        <source>More accessibility for everyone, thanks to the three dock levels !</source>
        <translation>Mehr Benutzbarkeit für Alle, dank drei verschiedenen Docklevels !</translation>
    </message>
    <message>
        <location filename="../show.qml" line="109"/>
        <source>More than 60 software programs available, which allow the beginners to discover GNU/Linux.</source>
        <translation>Mehr als 60 Softwares sind verfügbar, damit die Anfänger GNU/Linux entdecken dürfen.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="130"/>
        <source>More than 10 edutainment software programs for training, including Kiwix the offline Wikipedia reader.</source>
        <translation>Mehr als 10 spielerisch-pädagogischen Softwares für die Ausbildung, einschließlich der offline Wikipedia Reader Kiwix.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="151"/>
        <source>Emmabuntus supports social and environmental projects through the Lilo ethical and solidarity research engine.</source>
        <translation>Dank dem ethischen und solidarischen Webbrowser Lilo, unterstützt Emmabuntüs Sozial -und Umweltprojekte.</translation>
    </message>
        <message>
        <location filename="../show.qml" line="171"/>
        <source>Emmabuntus collaborates with more than 10 associations, particularly in Africa, in order to reduce the digital divide.</source>
        <translation>Emmabuntüs arbeitet insbesondere in Afrika mit mehr als 10 Vereinen zusammen, um die digitale Kluft zu überbrücken.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="191"/>
        <source>Emmabuntus together with YovoTogo and JUMP Lab'Orione have equipped and run 32 computer rooms in Togo.</source>
        <translation>Nachdem sie zusammen in Togo 32 Computerräume ausgerüstet haben, verwalten Emmabuntüs YovoTogo und JUMP Lab'Orione diese Einrichtungen.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="211"/>
        <source>Emmabuntus is also a refurbishing key, based on Ventoy,&lt;br/&gt;allowing you to quickly and easily refurbish a computer with GNU/Linux.</source>
        <translation>Emmabuntüs ist auch ein Refurbishing-Schlüssel, der auf Ventoy&lt;br/&gt;basiert und es Ihnen ermöglicht, einen Computer mit GNU/Linux schnell und einfach zu renovieren.</translation>
    </message>
</context>
</TS>
