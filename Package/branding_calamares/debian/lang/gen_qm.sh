#!/bin/bash

# Il faut installer le paquet qt4-default
# sudo apt install qtchooser qttools5-dev-tools

NAME=calamares-debian

lrelease ${NAME}_en.ts
lrelease ${NAME}_fr.ts
lrelease ${NAME}_es.ts
lrelease ${NAME}_de.ts
lrelease ${NAME}_pt.ts
lrelease ${NAME}_it.ts
lrelease ${NAME}_da.ts
