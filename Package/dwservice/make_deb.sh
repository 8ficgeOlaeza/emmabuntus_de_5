#!/bin/bash

nom=$(find . -type f -a -name 'control' -exec grep -H Package: {} \; |cut -d ' ' -f2)
version=$(find . -type f -a -name 'control' -exec grep -H Version: {} \; |cut -d ' ' -f2)
arch=$(find . -type f -a -name 'control' -exec grep -H Architecture: {} \; |cut -d ' ' -f2)

if [ ${nom} != "" ] && [ ${version} != "" ] && [ ${arch} != "" ] ; then
    dpkg-deb -b ${nom} ${nom}_${version}_${arch}.deb
else
    echo "Erreur dans l'identification paquet"
fi


