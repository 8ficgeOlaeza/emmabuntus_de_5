## Emmabuntus Debian Edition 5 packages ##

########################################################################################################
# Drivers & Services
########################################################################################################

lightdm zenity  feh friendly-recovery yad

numlockx openssh-client libpam-mount-bin curl debconf-utils

# Suppression de samba

# Installation de XCompMgr pour gérer la transparence de Cairo-dock sous LXDE et OpenBox
# xcompmgr ne plus installé car pb error 143: BadPicture request 139 minor 8 serial
picom

# prise en charge des téléphones portables, et auto-détection du Bluetooth
python3-bluez

software-properties-common task-xfce-desktop util-linux whois pmount xdg-user-dirs

gnupg user-setup desktop-base locales console-setup  console-data xorg
acpi x11-utils x11-apps systemd accountsservice sudo xdg-utils anacron xserver-xorg-input-synaptics
xserver-xorg-input-all xserver-xorg-input-joystick xserver-xorg-input-kbd xserver-xorg-input-mouse
xserver-xorg-input-wacom xserver-xorg-input-evdev
xserver-xorg-video-all xserver-xorg-video-cirrus xserver-xorg-video-intel
xserver-xorg-video-mga
xserver-xorg-input-evdev xserver-xorg-legacy

xserver-xorg-video-neomagic xserver-xorg-video-r128
xserver-xorg-video-savage xserver-xorg-video-siliconmotion xserver-xorg-video-sisusb xserver-xorg-video-tdfx
xserver-xorg-video-mach64 xserver-xorg-video-trident xserver-xorg-video-qxl

# Environnement XFCE
xfce4 xfce4-terminal xfce4-battery-plugin xfce4-weather-plugin xfce4-whiskermenu-plugin
xfce4-cpufreq-plugin xfce4-cpugraph-plugin xfce4-datetime-plugin
xfce4-mount-plugin xfce4-notifyd xfce4-mailwatch-plugin xfce4-xkb-plugin
xfce4-places-plugin xfce4-power-manager-plugins
xfce4-sensors-plugin xfce4-settings xfce4-taskmanager
thunar-archive-plugin thunar-volman thunar-media-tags-plugin
tumbler tumbler-plugins-extra mugshot

# Environnement LXQt
lxqt featherpad-l10n libfm-qt-l10n liblxqt-l10n lximage-qt-l10n
lxqt-about-l10n lxqt-admin-l10n lxqt-config-l10n lxqt-globalkeys-l10n lxqt-notificationd-l10n
lxqt-openssh-askpass-l10n lxqt-panel-l10n lxqt-policykit-l10n lxqt-powermanagement-l10n lxqt-runner-l10n
lxqt-session-l10n lxqt-sudo-l10n meteo-qt-l10n pcmanfm-qt-l10n qterminal-l10n qttranslations5-l10n
pavucontrol-qt qlipper ark

exfat-fuse libxss1

xdg-user-dirs
bash bash-completion command-not-found libnotify-bin usbutils xsltproc
libmtp9 libmtp-runtime jmtpfs mtp-tools
os-prober menu menulibre
laptop-detect xterm
polkitd-pkla

## Gestion des paquets
vrms gdebi synaptic lsb-release apt-xapian-index apt-transport-https lintian dkms unattended-upgrades apt-listchanges powermgmt-base

## authentification
gnome-system-tools

## themes
gtk2-engines-murrine gtk2-engines-pixbuf
arc-theme blackbird-gtk-theme papirus-icon-theme
lightdm-gtk-greeter-settings qt5-style-plugins

###
open-vm-tools-desktop

## codecs
flac wavpack lame libasound2-plugins
gstreamer1.0-alsa gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-tools gstreamer1.0-pulseaudio

## drivers libres
firmware-linux-free alsa-firmware-loaders

firmware-b43-installer b43-fwcutter firmware-b43legacy-installer

## drivers non-libres
firmware-linux firmware-linux-nonfree firmware-misc-nonfree firmware-ralink intel-microcode iucode-tool firmware-realtek
amd64-microcode bluez-firmware firmware-atheros firmware-bnx2 firmware-bnx2x
firmware-brcm80211 firmware-intel-sound firmware-iwlwifi firmware-libertas
firmware-myricom firmware-netxen firmware-qlogic firmware-zd1211
firmware-ath9k-htc

libblockdev-crypto2 cryptsetup-initramfs

# no deb12 firmware-intelwimax firmware-ipw2x00


########################################################################################################
# Communication
########################################################################################################

firefox-esr thunderbird pidgin transmission falkon jami

## Réseau
iproute2 iptables net-tools wpasupplicant wireless-tools ntpsec pppoeconf

#if ARCHITECTURES amd64
filezilla
#endif

# no deb12 telepathy-haze telepathy-salut telepathy-rakia

network-manager-gnome network-manager-openvpn-gnome network-manager-vpnc-gnome

########################################################################################################
# Bureautique
########################################################################################################

abiword gnumeric xfce4-dict calibre gscan2pdf pdfarranger
libreoffice  libreoffice-gtk3 libreoffice-style-elementary
geany unpaper djvulibre-bin gocr pdftk gnote scribus

verbiste-gtk

qpdfview qpdfview-translations qpdfview-djvu-plugin qpdfview-ps-plugin

galculator

# Synthese vocale pour Libre Office des écoles
xclip alsa-utils libpopt0

#if ARCHITECTURES amd64
python3-unrardll
#endif

########################################################################################################
# Son
########################################################################################################

alsa-utils alsa-tools
clementine pulseaudio swh-plugins pavucontrol audacity

gpodder quodlibet asunder

########################################################################################################
# Vidéo
########################################################################################################

vlc vlc-l10n dvgrab kaffeine kdenlive libdv4 dvdauthor vokoscreen-ng guvcview handbrake

# Paquets pour kdenlive
breeze-icon-theme frei0r-plugins

########################################################################################################
# Photo
########################################################################################################

gthumb inkscape xfce4-screenshooter imagemagick librsvg2-bin gimp drawing

#if ARCHITECTURES amd64
darktable
#endif

########################################################################################################
# Gravure
########################################################################################################

dvd+rw-tools xfburn cdrdao icedax

########################################################################################################
# Loisirs
########################################################################################################

sweethome3d supertux homebank gnome-sudoku
minetest minetest-mod-moreblocks minetest-mod-moreores minetest-mod-pipeworks minetest-mod-worldedit
minetest-mod-mesecons minetest-mod-quartz minetest-mod-basic-materials
minetest-mod-homedecor minetest-mod-nether  minetest-mod-unified-inventory
minetest-mod-mobs-redo gbrainy

#if ARCHITECTURES amd64
tuxguitar
#endif

########################################################################################################
# Education
########################################################################################################

tuxtype tuxmath tuxpaint tuxpaint-config gnuchess gnucap ktuberling gcompris-qt
aria2 kiwix kiwix-tools openboard

########################################################################################################
# Utilitaires
########################################################################################################

wine winetricks fonts-wine libwine playonlinux

#cairo-dock cairo-dock-plug-ins cairo-dock-xfce-integration-plug-in
# Réduire le nombre de plugin et utilisation de Gnome
cairo-dock-core cairo-dock-xfce-integration-plug-in
cairo-dock-alsamixer-plug-in cairo-dock-animated-icons-plug-in cairo-dock-dbus-plug-in cairo-dock-dialog-rendering-plug-in
cairo-dock-dnd2share-plug-in cairo-dock-dustbin-plug-in cairo-dock-folders-plug-in cairo-dock-gmenu-plug-in
cairo-dock-logout-plug-in cairo-dock-netspeed-plug-in cairo-dock-switcher-plug-in cairo-dock-rendering-plug-in
cairo-dock-showdesktop-plug-in cairo-dock-systray-plug-in cairo-dock-wifi-plug-in
# Ajoute nautilus
cairo-dock-shortcuts-plug-in

lm-sensors eject yelp mlocate keepassxc gnome-software gnome-packagekit apt-config-auto-update
package-update-indicator gnome-package-updater caffeine

# Ajout pour correction message d'erreur de gnome-software
fwupd at-spi2-core

# Paquets pour HPLip
hplip hplip-gui

cups cups-pdf cups-browsed hpijs-ppds printer-driver-c2050 printer-driver-c2esp printer-driver-cjet
printer-driver-escpr openprinting-ppds gutenprint-locales printer-driver-gutenprint
printer-driver-hpcups printer-driver-postscript-hp printer-driver-m2300w printer-driver-min12xxw
printer-driver-pnm2ppa printer-driver-ptouch printer-driver-sag-gdi printer-driver-splix
printer-driver-foo2zjs printer-driver-hpijs printer-driver-pxljr system-config-printer
magicfilter librecode0 recode lsb-release
python3-smbc

system-config-printer-udev

xfonts-100dpi xfonts-75dpi xfonts-base xfonts-terminus
fonts-liberation fonts-opendyslexic
fonts-crosextra-carlito fonts-crosextra-caladea

xfce4-power-manager arandr
xscreensaver xscreensaver-gl

# Ajout du bluetooth pour l'audio
bluetooth bluez blueman bluez-tools pulseaudio-module-bluetooth

usb-modeswitch modemmanager rfkill redshift-gtk geoclue-2.0 timeshift

# Ajout packages pour iPhone iPad
ideviceinstaller libimobiledevice-utils libimobiledevice6 libplist3 ifuse

python3-imobiledevice python3-plist

btrfs-progs squashfs-tools

smbclient samba-vfs-modules vlc-plugin-samba cifs-utils

thunar-gtkhash mediainfo-gui font-manager zram-tools calamares-settings-debian

mintstick

########################################################################################################
# Maintenance
########################################################################################################

ufw htop gparted exfatprogs hardinfo seahorse
zip p7zip-full dosfstools ntfs-3g mtools e2fsprogs f2fs-tools gpart gnome-disk-utility
gvfs gvfs-fuse gvfs-backends baobab lshw
flatpak file-roller bleachbit inxi mdadm dtrx gufw

########################################################################################################
# Accessibilité
########################################################################################################

vmg onboard

########################################################################################################
# Définition des langues de la distribution
########################################################################################################

locales
poppler-data

# Fr
firefox-esr-l10n-fr
thunderbird-l10n-fr
libreoffice-l10n-fr
wfrench
hunspell-fr
hyphen-fr
mythes-fr
debian-reference-fr


# En
firefox-esr-l10n-en-gb
thunderbird-l10n-en-gb
hunspell-en-us
wbritish
debian-reference-en


# Es
firefox-esr-l10n-es-es
thunderbird-l10n-es-es
libreoffice-l10n-es
myspell-es
wspanish


########################################################################################################
# Définition des langues partiels de la distribution
########################################################################################################


# It
firefox-esr-l10n-it
libreoffice-l10n-it


# De
firefox-esr-l10n-de
libreoffice-l10n-de

# Pt
firefox-esr-l10n-pt-pt
libreoffice-l10n-pt

# Da
firefox-esr-l10n-da
libreoffice-l10n-da

# Ja
firefox-esr-l10n-ja
libreoffice-l10n-ja
